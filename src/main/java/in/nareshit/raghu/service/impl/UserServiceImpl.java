package in.nareshit.raghu.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nareshit.raghu.constants.UserStatus;
import in.nareshit.raghu.entity.User;
import in.nareshit.raghu.repo.UserRepository;
import in.nareshit.raghu.service.IUserService;
import in.nareshit.raghu.util.AppUtil;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserRepository repo;
	
	@Override
	public Long saveUser(User user) {
		// TODO : PASSWORD ENCODING
		user.setStatus(UserStatus.INACTIVE.name());
		user.setPassword(AppUtil.genPwd());
		return repo.save(user).getId();
		// TODO : SENDIG EMAIL
	}

	@Override
	public Optional<User> findByEmail(String email) {
		return repo.findByEmail(email);
	}

	@Override
	public List<User> getAllUsers() {
		return repo.findAll();
	}

}
