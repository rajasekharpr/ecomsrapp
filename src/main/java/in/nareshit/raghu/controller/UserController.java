package in.nareshit.raghu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import in.nareshit.raghu.entity.User;
import in.nareshit.raghu.service.IUserService;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private IUserService service;

	@GetMapping("/register")
	public String showReg() {
		return "UserRegister";
	}
	
	@PostMapping("/save")
	public String saveUser(
			@ModelAttribute User user,
			Model model
			) 
	{
		Long id = service.saveUser(user);
		model.addAttribute("message", "User created with id "+id);
		return "UserRegister";
	}
	
	
	@GetMapping("/all")
	public String showAllUser(Model model) {
		model.addAttribute("list", service.getAllUsers());
		return "UserData";
	}
	
	@ResponseBody
	@GetMapping("/validateMail")
	public String validateEmail(
			@RequestParam("email")String email
			) 
	{
		String message ="";
		if(service.findByEmail(email).isPresent()) {
			message = email + " already exist";
		}
		return message;
	}
	
	//TODO: USER VALDIATION
}
